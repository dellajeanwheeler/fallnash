$(document).ready(function(){
  var navHeight = 40;

  $('#arts .slider').bxSlider();
  $('#music .slider').bxSlider();
  $('#food .slider').bxSlider();
  $('#family .slider').bxSlider();


$('#main').waypoint({
  offset: navHeight,
	handler: 
	function(direction) {
  		if (direction == "up"){
  			$('#nav').removeClass('stuck');

  		} if (direction == "down"){
  			$('#nav').addClass('stuck');
  		}
  	}
	});
  $('.smoothScroll').smoothScroll({offset: -navHeight}); 
  function changeNav(currentSection){
    $('#nav').removeClass('arts');
    $('#nav').removeClass('food');
    $('#nav').removeClass('music');
    $('#nav').removeClass('family');
    $('#nav').addClass(currentSection);
  }
  $('.section').waypoint({
    offset: navHeight,
    handler:
    function(direction){
    if (direction == "down"){ 
      changeNav(this.id);
    }
    }});
  
  $('.section').waypoint({
    offset: $.waypoints('viewportHeight')*(-1) +navHeight,
    handler:
    function(direction){
    if (direction == "up"){ 
      changeNav(this.id);
    }
    }});
});

